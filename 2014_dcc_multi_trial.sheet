## Sheet for the implementation of the Multi-trial decoding algorithm of
## Guruswami--Sudan.
##
## Written by Johan S. R. Nielsen, 2013--2014
##
## This algorithm was proposed in the article
##   Multi-Trial Guruswami--Sudan Decoding for Generalised Reed--Solomon Codes
##   Published in Designs, Codes and Cryptography, WCC special issue, 2014
##   by Johan S. R. Nielsen and Alexander Zeh
##
## The file includes all code used for simulations, including both debug
## output, timing and field operation counting. This means that the
## implementation of the raw multi-trial algorithm is somewhat muddled by
## such instrumentalisation, though an effort has been made to keep a clear
## separation within function bodies.
## Some helper functions for setting up codes and emulating received words with
## errors have been written. For usage, see the example codes after the
## implementation of the algorithm (search for "EXAMPLE").
##
## This implementation is in Sage (v. 6.2, http://sagemath.org). It depends on
## Codinglib by Johan S. R. Nielsen (http://jsrn.dk/codinglib). This
## sheet was tested and verified using the commit   33300a6   of codinglib:
## https://bitbucket.org/jsrn/codinglib/commits/33300a6bc03bb3d2e549215e0fe00882f300bb7f
##
## Please read the README of the code repository for a general description of
## the sheet format and usage instructions. Be aware that this is a
## simple research implementation. It is not well documented and
## probably not fully generic.

## LICENSE INFORMATION:
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

# For enum construction 
def enum(**enums):
    return type('Enum', (), enums)

def strip_weights_poly_matrix(M, DI):
    kc = DI.k-1
    return Matrix(ZZ, M.nrows(), M.ncols(), lambda i,j: M[i,j] - kc*j if M[i,j] >= 0 else -1)

def calculate_powers(p, s, partial=None):
    """Return a list l with l[i] = p^i"""
    if partial:
        l = partial
    else:
        l = [ p.parent().one()]
    start = len(l)
    for i in range(start,s+1):
        l.append(p*l[i-1])
    return l
    
def empty_counters(F, debug=0):
    if isinstance(F, CountingField):
        F.reset_counters()
        return { 'last_rowred': 0, 'total_rowred': 0, 'field_counting': True, 'time_counting': False,
                 'construction': 0, 'minimisation': 0, 'rootfinding': 0 }
    elif debug: # count time and row reductions
        return { 'last_rowred': 0, 'total_rowred': 0, 'field_counting': False, 'time_counting': True,
                 'construction': 0, 'minimisation': 0, 'rootfinding': 0, 'last_time': time.time() }
    else: # count nothing
        return None

def add_count(counters, category, DI):
    if counters is None:
        return
    if counters['field_counting']:
        counters[category] += DI.F.reset_counters()[1]
    elif counters['time_counting']:
        now = time.time()
        counters[category] += now-counters['last_time']
        counters['last_time'] = now

def forget_last_count(counters, DI):
    if counters is None:
        return
    if counters['field_counting']:
        DI.F.reset_counters()
    elif counters['time_counting']:
        counters['last_time'] = time.time()
    
def algorithm_name(alg):
    if alg=="mt":
        return "Multi-trial"
    else:
        return "Lee-O'Sullivan tau=%s" % alg[1]
    
def algorithm_name_short(alg):
    if alg=="mt":
        return "MT"
    else:
        return "LO%s" % alg[1]

### Implementation of the algorithm
class DecodingInstance:
    """Helper-class to generate a decoding instance, i.e. codeword, error and
    received word, as well as hold auxiliary variables as fields and calculate
    various derivative values.
    Constructor requires
    - C - the underlying (GRS) code
    - max_tau - the maximal decoding radius which will be considered (for precomputation)
    - nerrs - how many errors will be inserted
    
    Holds the following fields directly:
    - C - the underlying (GRS) code
    - F - the field (also accessible as C.F)
    - PF - F[x]
    - x - PF.gen()
    - n - the code length (C.n)
    - k - the dimension (C.k)
    - r - the received word vector
    - R - the Lagrangian through the received word, or the R_reduced if reencoding
    - G - the Lagrangian through all evaluation points, or through only the last n-k if reencoding
    - Hs - the n-k Lagrangian elementary functions for reencoding if reencoding
    """
    
    def __init__(self, C, max_tau, nerrs, reencoding=False):
        self.C = C
        self.F = C.F
        self.n = C.n
        self.k = C.k
        self.max_tau = max_tau
        self.max_params = gs_minimal_list(C.n,C.k,max_tau)
        PF.<x> = C.F[]
        self.PF = PF
        self.x = x
        self.reencoding = reencoding
        if not reencoding:
            self.yweight = C.k-1
            self.shift = 0
        else:
            self.yweight = -1
            self.shift = 1

        if not reencoding:
            #Precalculate values for regular decoding
            self.G = prod( x-a for a in C.alphas )
            self.L = self.PF.one()
            self.mRs = None
        else:
            #Precalculate values for reencoding decoding.
            #We will be reencoding on the first k positions always
            self.L = prod(x-alpha for alpha in C.alphas[:C.k])
            self.G = prod( x-a for a in C.alphas[C.k:] )
            hs = [ self.G//(x-a) for a in C.alphas[C.k:] ]
            self.Hs = [ hs[i-C.k]/(hs[i-C.k](C.alphas[i]))/self.L(C.alphas[i]) for i in range(C.k,C.n) ]
        self.Gs = calculate_powers(self.G, self.max_params[0])
        self.Ls = calculate_powers(self.L, self.max_params[1])

        #Generate codeword, error and received word
        self.c = C.random_codeword()
        self.f = PF(C.unencode(self.c).list())
        self.nerrs = nerrs
        self.e = random_error(self.n, self.F, nerrs)
        self.r = self.c + self.e

    def clear_R(self):
        self.R = None
        self.mRs = None

    def calculate_R(self):
        if self.R:
            return
        if not self.reencoding:
            self.R = self.PF.lagrange_polynomial([ (self.C.alphas[i], self.r[i]) for i in range(self.C.n) ])
        else:
            self.f_r = self.PF.lagrange_polynomial(zip(self.C.alphas[:self.k], self.r[:self.k]))
            self.r_r = vector([ self.F.zero() ]*self.k + 
                              [ self.r[i] - self.f_r(self.C.alphas[i]) for i in range(self.k,self.n) ])
            self.R = sum( self.r_r[i] * self.Hs[i-self.k] for i in range(self.k,self.n) )

    def mR_power(self, s):
        "Powers of minus R"
        if self.mRs is None or s>=len(self.mRs):
            self.mRs = calculate_powers(-self.R, s, self.mRs)
        return self.mRs[s]
        
    def tau(self, s, ell):
        return gs_decoding_radius(self.n, self.k, s=s, l=ell)[0]
        
    def N(self, s, ell, t=0):
        r"""Return $N_{s,\ell}^{(t)}."""
        tau = self.tau(s, ell)
        return s*(self.n-tau) - t*(self.k-1)

    def valid_decoding(self, sol, tau):
        if not self.reencoding:
            return sol.degree() < self.k and weight( self.C.encode(sol) - self.r ) <= tau
        else:
            return sol.degree() < self.k and weight( self.C.encode(sol) - self.r_r ) <= tau

Opers = enum(S1="S1", S2="S2", Root="Root")

def buildA(s, ell, DI, counters, weighted=True):
    """Build $A_{s,\\ell}W_\\ell$"""
    DI.calculate_R()
    G, Gs, R, Ls, shift, yweight = DI.G, DI.Gs, DI.R, DI.Ls, DI.shift, DI.yweight
    if s==1 and ell==1:
        M = matrix([[ G, 0 ],[ -R, DI.PF.one() ]])
    else:
        # This is already covered by gs_lee_osullivan_module, but for fair field
        # element counting, the powers of G are precalculated, and more care is
        # taken for the powers of -R
        # Powers of L have been added to support reencoding
        PFy.<y> = DI.PF[]
        yRpows = [ sum( binomial(ss, t)*y^t*DI.mR_power(ss-t)
                           for t in range(0,ss+1) ) for ss in range(0,s+1) ]
        ybasis = [ yRpows[i]*Gs[s-i] for i in range(0, s+1) ] \
                + [ Ls[i-s]*y^(i-s)*yRpows[s] for i in range(s+1, ell+1) ]
        def pad(lst):
            return lst + [0]*(ell+1-len(lst))
        modbasis = [ pad(yb.coeffs()) for yb in ybasis ]
        M = Matrix(DI.PF, modbasis)
    if weighted:
        module_apply_weights(M, [ ell*shift + j*yweight for j in range(ell+1) ])
    add_count(counters, 'construction', DI)
    return M

def buildC1(BW, (s, ell), DI, counters):
    """Given $B_{s,\\ell}W_\ell$, calculate $C1_{s,\\ell+1}W_{\ell+1}$."""
    Lpow, shift, yweight = DI.Ls[ell-s+1], DI.shift, DI.yweight
    def cell(i,j):
        if i<=ell:
            if j<=ell:
                return BW[i,j] * DI.x^shift
            else:
                return 0
        else:
            if j<ell-s+1:
                return 0
            else:
                coeff = binomial(s, j-(ell+1-s))
                return coeff * Lpow * DI.mR_power(min(i-j, s)) * DI.x^((ell+1)*shift + j*yweight)
    M = matrix(DI.PF, ell+2, ell+2, cell)
    add_count(counters, 'construction', DI)
    return M

def buildC2(BW, (s, ell), DI, counters):
    """Given $B_{s,\\ell}W_\\ell$, calculate $C2_{s+1,\\ell+1}W_{\ell+1}$."""
    Gs, R, shift, yweight = DI.Gs, DI.R, DI.shift, DI.yweight
    def cell(i,j):
        if i==0:
            return Gs[s+1]*DI.x^((ell+1)*shift) if j==0 else 0
        if j==0:
            return -R*BW[i-1,j]*DI.x^(shift)
        if j==ell+1:
            return BW[i-1,j-1]*DI.x^(shift+yweight)
        return BW[i-1,j-1]*DI.x^(shift+yweight) - R*BW[i-1, j]*DI.x^(shift)
    M = matrix(DI.PF, ell+2, ell+2, cell)
    add_count(counters, 'construction', DI)
    return M
    
def perform_rootfind(BW, s, ell, DI, debug=0):
    """Given $B_{s,\\ell}W_\ell$, extract a satisfactory bivariate polynomial
    and check if it has a good linear factor"""
    shift, yweight = DI.shift, DI.yweight
    i = 0
    # always pick the minimal row...
    i = module_minimal_row(BW)
    PFy.<y> = DI.PF[]
    BWrow = list(BW.row(i))
    Brow = [ BWrow[j]//DI.x^(ell*shift + j*yweight) for j in range(len(BWrow)) ]
    Q = PFy(Brow)
    # TODO: Fix
    if DI.reencoding:
        Q = phi_inv(Q, (s,ell), DI)
    if debug>0:
        print "Attempting root-finding with (s,ell)=(%s,%s)" % (s,ell)
        if debug==3:
            print "Q(x,y) = %s" % Q
    sols = rootfind_bivariate(Q, maxd=DI.k-1)
    return sols

def perform_rowreduction(M, DI, counters):
    rowreds = module_weak_popov(M)
    if counters:
        counters['last_rowred']   = rowreds
        counters['total_rowred'] += rowreds
        add_count(counters, 'minimisation', DI)

def debug_before(Mt, (s, ell), DI, counters, debug=0):
    """Print various debug output before row reduction"""
    if not debug:
        return
    if isinstance(Mt, tuple):
        (M, t) = Mt
        name = "C" + str(t)
        if t==1:
            ortho_bound = s*(DI.R.degree() - DI.yweight)
        else:
            ortho_bound = ell*(DI.R.degree() - DI.yweight)
    else:
        M = Mt
        name = "A"
        ortho_bound = None # don't check
    namef = "%s_(%s,%s)" % (name,s,ell)
    if debug >= 3:
        if ortho_bound:
            assert module_orthogonality_defect(M) == ortho_bound
        print "Degrees of %s" % namef
        print strip_weights_poly_matrix(poly_degs(M),DI)
        print "Degrees of %sW_%s" % (namef,ell)
        print poly_degs(M)
    if ortho_bound:
        rowred_bound = (ell+1)*(ortho_bound+ell+1)
        print "Reducing %sW_%s. Expected row reductions : %s" % (namef, ell, rowred_bound)
    if counters: # Remember that we shouldn't be counting all this stuff as spent opers
        forget_last_count(counters, DI)

def debug_after(BW, (s,ell), DI, counters, debug=0):
    """Print various debug output after row reduction"""
    if not debug:
        return
    print "Row reductions were %s" % counters['last_rowred']
    if debug >= 3:
        print "Degrees of B_(%s,%s)" % (s,ell)
        print strip_weights_poly_matrix(poly_degs(BW),DI)
        print "Degrees of B_(%s,%s)W_%s" % (s,ell,ell)
        print poly_degs(BW)
    if counters: # Remember that we shouldn't be counting all this stuff as spent opers
        forget_last_count(counters, DI)

# The different step types of the algorithm
def multitrial_firststep(DI, counters, debug=0):
    """Perform the initial step, constructing and minimising A_(1,1)"""
    (s, ell) = (1, 1)
    AW = buildA(s, ell, DI, counters, weighted=True)
    debug_before(AW, (s,ell), DI, counters, debug=debug)
    perform_rowreduction(AW, DI, counters)
    debug_after( AW, (s,ell), DI, counters, debug=debug)
    return AW

def multitrial_S1(BW, (s, ell), DI, counters, debug=0):
    CW = buildC1(BW, (s, ell), DI, counters)
    (s, ell) = (s, ell+1)
    debug_before((CW,1), (s,ell), DI, counters, debug=debug)
    perform_rowreduction(CW, DI, counters)
    debug_after( CW, (s,ell), DI, counters, debug=debug)
    return CW

def multitrial_S2(BW, (s, ell), DI, counters, debug=0):
    CW = buildC2(BW, (s, ell), DI, counters)
    (s, ell) = (s+1, ell+1)
    debug_before((CW,2), (s,ell), DI, counters, debug=debug)
    perform_rowreduction(CW, DI, counters)
    debug_after( CW, (s,ell), DI, counters, debug=debug)
    return CW

def multitrial_root(BW, (s,ell), DI, counters, debug=0):
    if debug:
        print "Attempting root-finding at tau = %s, (s,ell) = (%s, %s)" % (DI.tau(s,ell),s,ell)
    roots = perform_rootfind(BW, s, ell, DI, debug=debug)
    sols = [ root for root in roots
                if DI.valid_decoding(root, DI.tau(s,ell)) ]
    add_count(counters, 'rootfinding', DI)
    return sols

def multitrial_decode(opers, DI, debug=0):
    counters = empty_counters(DI.F, debug)
    BW = multitrial_firststep(DI, counters, debug=debug)
    (s, ell) = (1,1)
    sols = []
    for oper in opers:
        if oper==Opers.S1:
            BW = multitrial_S1(BW, (s,ell), DI, counters, debug=debug)
            (s, ell) = (s, ell+1)
        elif oper==Opers.S2:
            BW = multitrial_S2(BW, (s,ell), DI, counters, debug=debug)
            (s, ell) = (s+1, ell+1)
        elif oper==Opers.Root:
            sols = multitrial_root(BW, (s,ell), DI, counters, debug=debug)
            if sols != []:
                break
        else:
            raise Exception("Non-valid operation: %s" % oper)
    if debug and counters:
        print "Performed %s row reductions in total" % counters['total_rowred']
    if sols != []:
        ret = sols
    else:
        if debug:
            print "No solutions found"
            ret = BW
        else:
            ret = []
    if counters:
        return (ret, counters)
    else:
        return ret

### Various helper functions
def operation_queue_eager_root(DI, max_tau=None):
    """Return a list of operations for the decoding instance which rootfinds every time the decoding radius has increased.
    TODO: Is it better to do all D operations first, then C, or the other way around, or a mix? Currently, first Cs.
    """
    opers = [ Opers.Root ] # Begin with rootfinding at (s,ell) = (1,1) 
    ldr = list_decoding_range(DI.n, DI.n-DI.k+1)
    if max_tau==None:
        max_tau = ldr[1]
    else:
        max_tau = min(max_tau, ldr[1])
    (s,ell) = (1,1)
    for tau in range(ldr[0], max_tau+1):
        (s_t,ell_t) = gs_minimal_list(DI.n,DI.k,tau)
        if s == s_t and ell == ell_t:
            continue
        count2 = s_t - s
        count1 = ell_t - ell - count2
        assert count1 >= 0 and count2 >= 0
        opers.extend([Opers.S1]*count1)
        opers.extend([Opers.S2]*count2)
        opers.append(Opers.Root)
        (s,ell) = (s_t,ell_t)
    return opers

def sum_field_ops(counter):
    return counter['minimisation'] + counter['rootfinding'] + counter['construction']

def phi_inv(Q, (s,ell), DI):
    """The inverse of the phi-mapping used for reencoding"""
    Qtr = Q.list()
    sh, ellh = min(s+1, len(Qtr)) , min(ell+1, len(Qtr))
    Qt = [ Qtr[i]*DI.Ls[s-i] for i in range(sh) ] + [ Qtr[i]//DI.Ls[i-s] for i in range(s+1,ellh) ]
    return Q.parent()(Qt)
             
def test_leeosullivan(DI, tau, debug=0):
    DI.clear_R()
    counters = empty_counters(DI.F, debug)
    (s,ell) = gs_minimal_list(DI.n,DI.k,tau)
    DI.calculate_R()
    Abig = buildA(s, ell, DI, counters, weighted=True)
    perform_rowreduction(Abig, DI, counters)
    roots = perform_rootfind(Abig, s, ell, DI, debug=debug)
    sols = [ root for root in roots
             if DI.valid_decoding(root, DI.tau(s,ell)) ]
    if DI.reencoding:
        sols = [ sol + DI.f_r for sol in sols ]
    assert DI.f in sols , "Sent information poly not found"
    if counters:
        add_count(counters, 'rootfinding', DI)
        return counters

def test_multitrial(DI, tau, debug=0):
    DI.clear_R()
    opers = operation_queue_eager_root(DI, max_tau=tau)
    if debug>0:
        print "The steps multi-trial will take are:\n%s" %  opers
    ret = multitrial_decode(opers, DI, debug=debug)
    if isinstance(ret, tuple):
        sols, counters = ret
    else:
        sols = ret
        counters = None
    if is_matrix(sols):
        assert False, "Solution not found"
    else:
        if DI.reencoding:
            sols = [ sol + DI.f_r for sol in sols ]
        if sols == [DI.f]:
            if debug > 0:
                print "The constructed solution was only found"
                if debug >= 3:
                    print DI.f
        else:
            if not DI.f in sols:
                assert sols != [], "No solutions were returned!"
                assert all(weight(DI.r - DI.C.encode(sol)) < DI.nerrs for sol in sols), "Solution not found but not farther than the %s found solutions.\n Received was\n%s" % (len(sols), DI.r)
            if debug > 0:
                print "Found %s solutions. The constructed solution was among them" % len(sols)
                if debug >= 3:
                    print "%s" % sols
    return counters

def debug_info_code(C, tau):
    print "The code is %s over GF(%s)" % (C, C.F.cardinality())
    print "The evaluation points are %s" % C.alphas
    print "We are decoding up to %s errors" % tau

def debug_info_instance(DI):
    print "The sent codeword was\n\t%s" % DI.c
    print "The error was of weight %s:\n\t%s" % (weight(DI.e), DI.e)
    print "The received word was\n\t%s" % DI.r


def test_a_code(C, tau, nerrs, reencoding=False, debug=0):
    DI = DecodingInstance(C, tau, nerrs, reencoding=reencoding)
    if debug > 0:
        debug_info_code(C, tau)
        debug_info_instance(DI)
    mt_counters = test_multitrial(DI, tau, debug=debug)
    if debug > 0:
        print "Finished Multi-Trial. Running Lee--O'Sullivan."
    DI.clear_R()
    lo_counters = test_leeosullivan(DI, tau, debug=debug)
    if mt_counters:
        return (mt_counters, lo_counters)

def stat_a_code(C, tau, nerrs_list, trials, reencoding=False, debug=0):
    counts = { 'mt': { i: [] for i in nerrs_list } }
    lo_list = range((C.d-1)//2, tau+1)
    i=1
    while i<len(lo_list):
        if gs_params(C.n,C.k,lo_list[i]) == gs_params(C.n,C.k,lo_list[i-1]):
            del lo_list[i-1]
        else:
            i=i+1
    if debug > 0:
        debug_info_code(C, tau)
        print "Performing Lee-O'Sullivan at tau=", lo_list
    for tau_lo in lo_list:
        counts[('lo',tau_lo)] = { i: [] for i in nerrs_list }
    for nerrs in nerrs_list:
        print "-------- TESTING %s ERRORS ---------" % nerrs
        for i in range(trials):
            if debug>0:
                print "\t\t TRIAL %s " % i
            DI = DecodingInstance(C, tau, nerrs, reencoding=reencoding)
            if debug >= 2:
                debug_info_instance(DI)
            # Run the multi-trial
            if debug > 0:
                print "\tRunning Multi-trial"
            counts['mt'][nerrs].append( test_multitrial(DI, tau, debug=debug) )
            # Run LO for each decoding radius at least nerrs
            for tau_lo in lo_list:
                if tau_lo >= nerrs:
                    if debug > 0:
                        print "\tRunning Lee--O'Sullivan with target %s" % tau_lo
                    DI.clear_R()
                    counts[('lo', tau_lo)][nerrs].append(
                        test_leeosullivan(DI, tau_lo, debug=debug) )
    return counts
        


### EXAMPLE (16, 4) code
import time

before = time.time()
(n,k) = 16, 4
tau = 8
F = GF(17)
C = GRS(F, n, k, alphas=F.list()[1:n+1])
nerrs = tau
counters = test_a_code(C, tau, nerrs, reencoding=True, debug=1)
if counters:
    print "Multi-trial multiplications: %s. Lee--O'Sullivan multiplications: %s" % counters
print "Took %s seconds" % (time.time() - before)


### EXAMPLE (64, 25) code

before = time.time()
(n,k) = 64,25
tau = list_decoding_range(n,n-k+1)[0]
tau = 20   #(s,ell)=(2,3)
F = GF(67)
C = GRS(F, n, k, alphas=F.list()[1:n+1])
print [ (t, gs_minimal_list(n,k,t)) for t in range(*list_decoding_range(n,n-k+1)) ]
###
nerrs = tau
counters = test_a_code(C, tau, nerrs, debug=3)
if counters:
    print "Multi-trial row count:\n%s\nLee--O'Sullivan count:\n%s" % (counters[0], counters[1])
print "Took %s seconds" % (time.time() - before)


### EXAMPLE (255, 120) code

before = time.time()
(n,k) = 255, 120
print [ (t, gs_minimal_list(n,k,t)) for t in range(*list_decoding_range(n,n-k+1)) ]
print gs_decoding_radius(n,k,l=4, s=2) > gs_decoding_radius(n,k,l=3,s=2)
###
tau = 74
nerrs = tau
F = GF(256,'a')
C = GRS(F, n, k, alphas=F.list()[1:n+1])
counters = test_a_code(C, tau, nerrs, debug=1)
if counters:
    print "Multi-trial row count:\n%s\nLee--O'Sullivan count:\n%s" % (counters[0], counters[1])
print "Took %s seconds" % (time.time() - before)


    
#
### Simulation over many runs and number of errors
#
ntrials = 1000
n,k = 16, 4
F = CountingField(GF(17))
C = GRS(F, n, k,  alphas=F.list()[1:n+1])
reencoding = True
tau = 8
nerrs_list = range(1, tau+1)

(n,k) = 255, 120
tau = 74
nerrs = tau
F = GF(256,'a')
C = GRS(F, n, k, alphas=F.list()[1:n+1])
reencoding = True
nerrs_list = range((C.d-1)//2, tau+1)

# n,k = 64, 25
# F = GF(67)
# C = GRS(F, n, k,  alphas=F.list()[1:n+1])
# reencoding = False
# tau = 23
# nerrs_list = range(1, tau+1)

counts = stat_a_code(C, tau, nerrs_list, ntrials, reencoding=reencoding, debug=1)

### Saving/Loading data
import pickle
def data_stubname():
    test_type = "" if isinstance(F, CountingField) else "secs_"
    return r'stats_(%s,%s)_%s%sT%s_' % (n,k,test_type,"" if not reencoding else "Re_",ntrials)
def data_filename():
    stub = data_stubname()
    return r'%s%s.dat' % (stub, int(time.time()))
def save_data():
    filename = data_filename()
    print "Saving the data to file ", filename
    afile = open(filename, 'wb')
    pickle.dump(counts, afile)
    afile.close()
def load_data():
    import glob
    filestub = data_stubname()
    data_files = glob.glob(filestub + "*.dat")
    if len(data_files) != 1:
        if data_files:
            raise Exception("More than one data file matching stub '%s'" % filestub)
        else:
            raise Exception("No data file matching stub '%s'" % filestub)
    filename = data_files[0]
    print "Loading data from file ", filename
    afile = open(filename, 'r')
    counts = pickle.load(afile)
    afile.close()
    return counts


### Graph the simulation (sum of mults)
def graph_stat(calc, alg):
    options = { 'color': 'black' , 'plotjoined': True }
    if isinstance(alg, tuple):
        taus =  [ alg_[1] for alg_ in counts if isinstance(alg_, tuple) ]
        taus.sort()
        if alg[1] == max(taus):
            options['legend_label'] = ltext("Lee-O'Sullivan $\\tau = %s" % ",".join([str(t) for t in taus]) )
        options['linestyle'] = "--"
    elif alg=="mt":
        options['legend_label'] = ltext("Multi-trial")
        options['linestyle'] = "-"
    else:
        raise Exception("Invalid algorithm")
    data = []
    for nerrs in nerrs_list:
        trials = counts[alg][nerrs]
        if len(trials) > 0:
            res = float(sum( calc(count) for count in trials ))/len(trials)
            data.append((nerrs, res))
    g = list_plot(data, **options)
    return g
filename_add = ""
filename_add += "_R" if reencoding else ""
if isinstance(F, CountingField):
    ytitle = "multiplications"
else:
    ytitle = "seconds"
    filename_add += "_secs"

g = Graphics()
for alg in counts:
    g+= graph_stat(sum_field_ops, alg)
#set unified y-axis
if isinstance(F, CountingField) and (n,k) == (16, 4):
    ymax = 35000
else:
    ymax = None
gs = plot(g, ymin=100, ymax=ymax, scale='semilogy', axes_labels=[r"$\varepsilon$","$\\mathrm{%s}$" % ytitle], tick_formatter='latex', fontsize=20,
          ticks=[nerrs_list,None])
gs += list_plot([(1,100), (1,35000)], color='white') # Force ymin=100 , ymax=35000
gs.set_legend_options(loc='lower right', font_size=20, borderpad=.3, borderaxespad=0, fancybox=True)
show(gs, legend_handlelength=1.5)


### Delete the min-dist count of Lee--O'Sullivan
del counts[("lo", (C.d-1)//2)]

### Save the above graph
print "Saving multiplication graph"
gs.save('multsL_%d_%d%s.eps' % (n,k, filename_add), legend_handlelength=1.5)


### Graph the non-minimisation cost
g = Graphics()
def nonmin(counter):
    return counter["construction"] + counter["rootfinding"]
for alg in counts:
    g+= graph_stat(nonmin, alg)
gs = plot(g, ymin=0, axes_labels=[r"$\tau$",r"$\mathrm{multiplications}$"], tick_formatter='latex')
title = ltext("$(n,\\ k,\\ d)=(%d,\\ %d,\\ %d)$" % (n,k,C.d))
g.set_legend_options(loc='upper center')
show(gs, legend_title=title, legend_handlelength=3)

### Save the above graph
print "Saving non-minimisation graph"
gs.save('nonmin_%d_%d%s.eps' % (n,k, filename_add), legend_title=title, legend_handlelength=3)


### Graph the _relative_ non-minimisation cost
g = Graphics()
def relative(counter):
    nonmin = counter["construction"] + counter["rootfinding"]
    return 100.*float(nonmin)/(nonmin+counter["minimisation"])
for alg in counts:
    g+= graph_stat(relative, alg)
gs = plot(g, ymin=0, axes_labels=[r"$\tau$",r"$\%$"], tick_formatter='latex')
title = ltext("$(n,\\ k,\\ d)=(%d,\\ %d,\\ %d)$" % (n,k,C.d))
g.set_legend_options(loc='upper center')
show(gs, legend_title=title, legend_handlelength=3)

### Save the above graph
print "Saving relative graph"
gs.save('relative_%d_%d%s.eps' % (n,k, filename_add), legend_title=title, legend_handlelength=3)


### Graph the row reductions
g = Graphics()
def rowreds(counter):
    return counter["total_rowred"]
for alg in counts:
    g+= graph_stat(rowreds, alg)
gs = plot(g, ymin=0, axes_labels=[r"$\tau$",r"$\mathrm{row reds}$"], tick_formatter='latex',
          ticks=[nerrs_list,[40*i for i in range(1,6)]])
title = ltext("$(n,\\ k,\\ d)=(%d,\\ %d,\\ %d)$" % (n,k,C.d))
g.set_legend_options(loc='upper center')
show(gs, legend_title=title, legend_handlelength=3)

### Save the above graph
print "Saving row reduction graph"
gs.save('rowred_%d_%d%s.eps' % (n,k, filename_add), legend_title=title, legend_handlelength=3)


### Poor-man's box-plot
def box_stat(calc, alg):
    opt = { 'color': 'black' , 'plotjoined': True }
    g = Graphics()
    calcs = dict()
    for nerrs in nerrs_list:
        trials = counts[alg][nerrs]
        if len(trials) > 0:
            calcs[nerrs] = [ calc(count) for count in trials ]
    def plot_aggregate(agg, opt):
        data = []
        for nerrs in nerrs_list:
            if nerrs in calcs and len(calcs[nerrs]) > 0:
                res = agg(calcs[nerrs])
                data.append((nerrs, res))
        return list_plot(data, **opt)
    # AVERAGE
    opt['legend_label'] = "Average"
    opt['linestyle'] = "-"
    g+= plot_aggregate(lambda counts: float(sum(counts))/len(counts), opt)
    # MAX
    opt['legend_label'] = "Max"
    opt['marker'] = "^"
    g+= plot_aggregate(lambda counts: max(counts), opt)
    # MIN
    opt['legend_label'] = "Min"
    opt['marker'] = "v"
    g+= plot_aggregate(lambda counts: min(counts), opt)
    # MEDIAN
    import scipy.stats
    opt['legend_label'] = "Median"
    opt['linestyle'] = "--"
    del opt['marker']
    g+= plot_aggregate(lambda counts: scipy.stats.scoreatpercentile(counts, int(50)), opt)
    # 75% percentile
    opt['legend_label'] = "25/75% Percentile"
    opt['marker'] = "o"
    g+= plot_aggregate(lambda counts: scipy.stats.scoreatpercentile(counts, int(75)), opt)
    # 25% percentile
    del opt['legend_label']
    opt['marker'] = "o"
    g+= plot_aggregate(lambda counts: scipy.stats.scoreatpercentile(counts, int(25)), opt)
    g.set_legend_options(title="%s" % algorithm_name(alg), handlelength=3)
    return g

####
alg = "mt"
#alg = ("lo", 74)
gs = box_stat(sum_field_ops, alg)
show(gs)

### Save the above graph
print "Saving aggregates graph"
gs.save('mults_aggs_%s_%d_%d%s.eps' % (algorithm_name_short(alg), n,k, filename_add))



### Graph relative improvement gained from Reencoding: i.e. % of operations/time saved
reencoding = False
counts = load_data()
reencoding = True
counts_R = load_data()
relgain = dict()
for alg in counts.keys():
    relgain[alg] = dict()
    for nerrs in counts[alg]:
        trials, trials_R = counts[alg][nerrs], counts_R[alg][nerrs]
        if len(trials) > 0:
            asum   = sum(sum_field_ops(c) for c in trials)/len(trials)
            asum_R = sum(sum_field_ops(c) for c in trials_R)/len(trials_R)
            relgain[alg][nerrs] = float(asum-asum_R)/asum

g = Graphics()
options = { 'color': 'black' , 'plotjoined': True }
for alg in counts:
    gains = relgain[alg]
    g = list_plot(gains, legend_label = alg, **options)
gs = plot(g, ymin=0, axes_labels=[r"$\tau$",r"$\mathrm{rel. gain}$"], tick_formatter='latex')
g.set_legend_options(loc='upper center')
show(gs, legend_title=title, legend_handlelength=3)


#
### Testing the conjecture on the optimal paths
#
def test_code(n, k, get_params):
    ldr = list_decoding_range(n, n-k+1)
    (s,ell) = (1,1)
    for tau in range(ldr[0], ldr[1]+1):
        hs,hell = get_params(n,k,tau)
        if hs < s or hell < ell or hs-s > hell-ell:
            raise Exception("No good for (n,k,tau)=(%i,%i,%i)" % (n, k, tau))
        s,ell = hs,hell
    print "Everything ok; %i decoding radii" % (ldr[1]-ldr[0]+2)
def do_test():
    n = randint(10, 10000)
    k = randint(2, int((n-1)*.9))
    test_code(n,k, gs_minimal_list)
    test_code(n,k, gs_params)
while True:
    do_test()
